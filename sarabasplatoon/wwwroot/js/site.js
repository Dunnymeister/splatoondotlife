﻿var splatApp = angular.module('splatApp', ['pascalprecht.translate', 'slick']);

var IMAGE_ROOT = "/images/stages/";

var stages = [
    {
        Name: "STAGE_CANAL",
        Theme: "THEME_RAIN",
        Description: "THEME_RAIN_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_SKATEPARK",
        Theme: "THEME_BUCKETS",
        Description: "THEME_BUCKETS_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_MART",
        Theme: "THEME_BURSTBOMB",
        Description: "THEME_BURSTBOMB_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_THEREEF",
        Theme: "THEME_ROLLER",
        Description: "THEME_ROLLER_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_FITNESS",
        Theme: "THEME_AUTOBOMB",
        Description: "THEME_AUTOBOMB_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_MAINSTAGE",
        Theme: "THEME_NINJA",
        Description: "THEME_NINJA_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_PUMPTRACK",
        Theme: "THEME_BALLER",
        Description: "THEME_BALLER_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_ACADEMY",
        Theme: "THEME_SPRINKLERS",
        Description: "THEME_SPRINKLERS_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_SHIPYARD",
        Theme: "THEME_DYNAMO",
        Description: "THEME_DYNAMO_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_TOWERS",
        Theme: "THEME_BEACONS",
        Description: "THEME_BEACONS_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_PORT",
        Theme: "THEME_AEROSPRAY",
        Description: "THEME_AEROSPRAY_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_MARIA",
        Theme: "THEME_BRUSH",
        Description: "THEME_BRUSH_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_DOME",
        Theme: "THEME_STINGRAY",
        Description: "THEME_STINGRAY_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_CANAL",
        Theme: "THEME_INKJET",
        Description: "THEME_INKJET_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_SKATEPARK",
        Theme: "THEME_CLASH",
        Description: "THEME_CLASH_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_MART",
        Theme: "THEME_SNG",
        Description: "THEME_SNG_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_THEREEF",
        Theme: "THEME_BOMBRUSH",
        Description: "THEME_BOMBRUSH_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_FITNESS",
        Theme: "THEME_BUMPER",
        Description: "THEME_BUMPER_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_MAINSTAGE",
        Theme: "THEME_BRELLA",
        Description: "THEME_BRELLA_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_PUMPTRACK",
        Theme: "THEME_DUELIE",
        Description: "THEME_DUELIE_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_ACADEMY",
        Theme: "THEME_INKMINE",
        Description: "THEME_INKMINE_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_SHIPYARD",
        Theme: "THEME_SPLATLING",
        Description: "THEME_SPLATLING_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_TOWERS",
        Theme: "THEME_GOOTUBER",
        Description: "THEME_GOOTUBER_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_PORT",
        Theme: "THEME_STINA",
        Description: "THEME_STINA_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_MARIA",
        Theme: "THEME_INKARMOR",
        Description: "THEME_INKARMOR_DESCRIPTION",
        Mode: "MODE_TURF"
    },
    {
        Name: "STAGE_DOME",
        Theme: "THEME_GRIZZCO",
        Description: "THEME_GRIZZCO_DESCRIPTION",
        Mode: "MODE_TURF"
    },
];

// Create a new identifer by adding it to the "en" and "jp" translation arrays below
splatApp.config(['$translateProvider', function ($translateProvider) {
    $translateProvider
        .translations('en', {
            UI_DATE: '2nd December 2017',
            UI_SITETITLE: 'Merry Turfmas!!!',
            UI_TOGGLEICON: '<span class="flag-icon flag-icon-jp"></span>',
            UI_STAGENUMBER: 'Round',
            UI_LANGUAGESELECT: 'Language',
            STAGE_THEREEF: 'The Reef',
            STAGE_FITNESS: 'Musselforge Fitness',
            STAGE_MAINSTAGE: 'Starfish Mainstage',
            STAGE_PUMPTRACK: 'Humpback Pump Track',
            STAGE_ACADEMY: 'Inkblot Art Academy',
            STAGE_SHIPYARD: 'Sturgeon Shipyard',
            STAGE_MARIA: 'Manta Maria',
            STAGE_CANAL: 'Snapper Canal',
            STAGE_MART: 'MakoMart',
            STAGE_MALL: 'Arowana Mall',
            STAGE_SKATEPARK: 'Blackbelly Skatepark',
            STAGE_RIG: 'Saltspray Rig',
            STAGE_UNDERPASS: 'Urchin Underpass',
            STAGE_WAREHOUSE: 'Walleye Warehouse',
            STAGE_DOME: 'Kelp Dome',
            STAGE_PORT: 'Port Mackerel',
            STAGE_DEPOT: 'Bluefin Depot',
            STAGE_TOWERS: 'Moray Towers',
            STAGE_CAMP: 'Camp Triggerfish',
            STAGE_HEIGHTS: 'Flounder Heights',
            STAGE_BRIDGE: 'Hammerhead Bridge',
            STAGE_MUSEUM: 'Museum D\'Alfonsino',
            STAGE_RESORT: 'Mahi-Mahi Resort',
            STAGE_PIT: 'Piranha Pit',
            STAGE_GAMES: 'Ancho-V Games',
            MODE_TURF: 'Turf War',
            MODE_ZONES: 'Splat Zones',
            MODE_TOWER: 'Tower Control',
            MODE_RAINMAKER: 'Rainmaker',
            THEME_GRIZZCO: 'Work Xmas Function',
            THEME_GRIZZCO_DESCRIPTION: 'Everyone must wear Grizzco headgear and Grizzco clothing. I wonder if Mr Grizz will give us a Christmas bonus~',
            THEME_INKARMOR: 'Sparkling Squiddies',
            THEME_INKARMOR_DESCRIPTION: 'Everyone must use a weapon with the ink armor special.',
            THEME_STINA: 'Stina Sensor',
            THEME_STINA_DESCRIPTION: 'Stina\'s Request! Everyone must use a weapon with the point sensor sub weapon.',
            THEME_GOOTUBER: 'Gootuber is the best! ...said no one ever',
            THEME_GOOTUBER_DESCRIPTION: 'Girl A: &quot;Gootuber\'s the best!!!!&quot;<br />Girl B: &quot;I can\'t hear you!!&quot;<br />Girl A: &quot;GOOTUBER IS THE BEEEEESSSSSST!!!!!!&quot;<br />Girl B: &quot;LALALA I CAN\'T HEAR YOU LALALALA&quot;<br />Girl A: &quot;LALALA stop.&quot;<br /><br />(Everyone must use the gootuber.)<br />*if you don\'t have the Gootuber you can use the Bamboozler',
            THEME_DUELIE: 'Duelie to the Death',
            THEME_DUELIE_DESCRIPTION: 'Everyone must use a duelie weapon.',
            THEME_BRELLA: 'Brella Band Festival',
            THEME_BRELLA_DESCRIPTION: 'Everyone must use a brella weapon.',
            THEME_SNG: 'Same Same but Different',
            THEME_SNG_DESCRIPTION: 'Everyone must use either a splattershot, nzap or .52 gal shooter weapon.',
            THEME_CLASH: 'Blaster Crash Course',
            THEME_CLASH_DESCRIPTION:'Everyone must use the luna blaster.',
            THEME_INKJET: '3\, 2\, 1\...Blast Off!',
            THEME_INKJET_DESCRIPTION: 'Everyone must use a weapon with the inkjet special.',
            THEME_STINGRAY: 'Not Killer Wail',
            THEME_STINGRAY_DESCRIPTION: 'Everyone must use a weapon with the stingray special.',
            THEME_BRUSH: 'Swabbing the Decks',
            THEME_BRUSH_DESCRIPTION: 'Everyone must use a brush weapon.',
            THEME_AEROSPRAY: 'Get Aeroed',
            THEME_AEROSPRAY_DESCRIPTION: 'Everyone must use an aerospray.',
            THEME_DYNAMO: 'Never Forgive the Dynamo',
            THEME_DYNAMO_DESCRIPTION: 'Everyone must use a dynamo roller.',
            THEME_BALLER: 'Hamster Ball Pump Track',
            THEME_BALLER_DESCRIPTION:'Everyone must use a weapon with the baller special.',
            THEME_NINJA: 'Dunny the Ninja 2',
            THEME_NINJA_DESCRIPTION: 'Let\'s Assasinate Dunny! Everyone must wear Ninja Squid clothes and Stealth Jump shoes.',
            THEME_AUTOBOMB: 'Autobombs Roll Out!',
            THEME_AUTOBOMB_DESCRIPTION: 'Everyone must use a weapon with the autobomb sub weapon.',
            THEME_BURSTBOMB: 'BB to the Face',
            THEME_BURSTBOMB_DESCRIPTION: 'Everyone must use a weapon with the burst bomb sub weapon.',
            THEME_BUCKETS: 'Blackbelly Buckets',
            THEME_BUCKETS_DESCRIPTION: 'Everyone must use a bucket weapon.',
            THEME_RAIN: 'Rain Rain Go Away',
            THEME_RAIN_DESCRIPTION: 'Everyone must use a weapon with the ink storm special.',
            THEME_BANNED: 'Banned Stage/Mode Combination',
            THEME_BANNED_DESCRIPTION: 'Let\'s play this map/mode combo since it doesn\'t show up in ranked!',
            THEME_ROLLER: 'Roller4Eva! 2',
            THEME_ROLLER_DESCRIPTION: 'Everyone must use a roller and only roll. Ok? Only rolling allowed!<br />OK? ONLY ROLLING ALLOWED!<br /><br /><small>.........Hikaru was squished.</small><br /><br /><strong>Note: You may swing the roller to paint walls in order to climb them!</strong>',
            THEME_BUMPER: 'Bumper Brush 2',
            THEME_BUMPER_DESCRIPTION: 'Everyone must use a brush, no swinging allowed, only rolling.',
            THEME_SPLATLING: 'Hikaru\'s Utopia 2',
            THEME_SPLATLING_DESCRIPTION: 'Everyone must use a splatling, it\'s splatling appreciation time!',
            THEME_INKMINE: 'IT\'S A TRAP!!',
            THEME_INKMINE_DESCRIPTION: 'Everyone must use a weapon with the ink mine sub weapon.',
            THEME_NOZZLENOSE: 'Reel Art',
            THEME_NOZZLENOSE_DESCRIPTION: 'Everyone must use a nozzlenose weapon.',
            THEME_SPRINKLERS: 'Sprinkler Art Academy',
            THEME_SPRINKLERS_DESCRIPTION: 'Everyone must use a weapon with the sprinkler sub weapon.',
            THEME_ELITRES: 'Dunny\'s Nightmare',
            THEME_ELITRES_DESCRIPTION: 'Everyone must use an E-litre.',
            THEME_BLASTERS: 'Fireworks!!',
            THEME_BLASTERS_DESCRIPTION: 'Everyone must use a blaster.',
            THEME_BOMBRUSH: 'Bomb Rush Blush 2',
            THEME_BOMBRUSH_DESCRIPTION: 'Everyone must use a weapon with a Bomb Rush special (Splat, Suction, Burst or Curling).',
            THEME_SPLATTERSHOTJR: 'Maus Training',
            THEME_SPLATTERSHOTJR_DESCRIPTION: 'Think like Maus. Be pretty like Maus. Be Maus!<br /><br />Everyone must use the Splattershot Jr.',
            THEME_SPLOOSHNEO: 'Krakenhead Bridge',
            THEME_SPLOOSHNEO_DESCRIPTION: 'Everyone must use the Neo Sploosh-o-matic.',
            THEME_BEACONS: 'Squid Jump Game 2',
            THEME_BEACONS_DESCRIPTION: 'Everyone must use a weapon with the beacon sub weapon.',
            THEME_INKZOOKA: 'Inkzooka Depot',
            THEME_INKZOOKA_DESCRIPTION: 'Everyone must use a weapon with the Inkzooka special.',
            THEME_INKSTRIKE: 'GO! INKSTRIKE!',
            THEME_INKSTRIKE_DESCRIPTION: 'Everyone must use a weapon with the Inkstrike special.',
            THEME_NAKOCHI: 'Everyone is Nakochi!',
            THEME_NAKOCHI_DESCRIPTION: 'When you woke up, you realised you had become... NAKOCHI! That person over there? They also turned into Nakochi! EVERYONE IS NAKOCHI!<br/><br/>Let the Nakochi Wars begin!<br /><br />You must wear the following gear:<ul><li>Head: Golf Visor</li><li>Clothing: Navy Striped LS</li><li>Shoes: Plum Casuals</li></ul>',
        })
        .translations('jp', {
            UI_DATE: '2017念　12月　2日',
            UI_SITETITLE: 'メリー　ナワバリ！！！',
            UI_TOGGLEICON: '<span class="flag-icon flag-icon-gb"></span>',
            UI_STAGENUMBER: 'ラウンド',
            UI_LANGUAGESELECT: '言語',
            STAGE_THEREEF: 'バッテラストリート',
            STAGE_FITNESS: 'フジツボスポーツクラブ',
            STAGE_MAINSTAGE: 'ガンガゼ野外音楽堂',
            STAGE_PUMPTRACK: 'コンブトラック',
            STAGE_ACADEMY: '海女美術大学',
            STAGE_SHIPYARD: 'チョウザメ造船',
            STAGE_MARIA: '	マンタマリア号',
            STAGE_CANAL: 'エンガワ河川敷',
            STAGE_MART: 'ザトウマーケット',
            STAGE_MALL: 'アロワナモール',
            STAGE_SKATEPARK: 'Bバスパーク',
            STAGE_RIG: 'シオノメ油田',
            STAGE_UNDERPASS: 'デカライン高架下',
            STAGE_WAREHOUSE: 'ハコフグ倉庫',
            STAGE_DOME: 'モズク農園 ',
            STAGE_PORT: 'ホッケふ頭',
            STAGE_DEPOT: 'ネギトロ炭鉱',
            STAGE_TOWERS: 'タチウオパーキング',
            STAGE_CAMP: 'モンガラキャンプ場',
            STAGE_HEIGHTS: 'ヒラメが丘団地',
            STAGE_BRIDGE: 'マサバ海峡大橋',
            STAGE_MUSEUM: 'キンメダイ美術館',
            STAGE_RESORT: 'マヒマヒリゾート＆スパ',
            STAGE_PIT: 'ショッツル鉱山',
            STAGE_GAMES: 'アンチョビットゲームズ',
            MODE_TURF: 'ナワバリバトル',
            MODE_ZONES: 'ガチエリア',
            MODE_TOWER: 'ガチヤグラ',
            MODE_RAINMAKER: 'ガチホコ',
            THEME_GRIZZCO: '仕事のクリスマスパーティー',
            THEME_GRIZZCO_DESCRIPTION: 'みんなはクマさんの帽子とクマさんの服を着てなければならない。クマさんは私たちにクリスマスの報酬をあげるかな～',
            THEME_INKARMOR: 'キラキライカしてる',
            THEME_INKARMOR_DESCRIPTION: 'みんなはインクアーマーを持っている武器を使ってなければならない',
            THEME_STINA: 'スティーナセンサー',
            THEME_STINA_DESCRIPTION: 'スティーナのリクエストだ！ みんなはポイントセンサーを持っている武器を使ってなければならない。',
            THEME_GOOTUBER: 'ソイチューバー最高！って誰も言わなかった',
            THEME_GOOTUBER_DESCRIPTION: '少女A:「ソイチューバーんあっ最高！！！！」<br />少女B:「聞こえないよ！」<br />少女A:「ソイチューバーんあああああああああああっ最高！！！！！！」<br />少女B:「ラララー 聞こえないよ〜 ラララー」<br />少女A:「ラララstop」<br /><br />(みんなはソイチューバーを使ってなければならない)',
            THEME_DUELIE: 'マニューバーミッション',
            THEME_DUELIE_DESCRIPTION: 'みんなはマニューバー武器を使ってなければならない',
            THEME_BRELLA: '傘カーニヴァル',
            THEME_BRELLA_DESCRIPTION: 'みんなはシェルター武器を使ってなければならない',
            THEME_SNG: '違いさは？',
            THEME_SNG_DESCRIPTION: 'みんなはスプラシューターとかN-ZAPとか.52ガロンとか使ってなければならない',
            THEME_CLASH: 'ブラスタークラッシュコース',
            THEME_CLASH_DESCRIPTION: 'みんなはルナブラスターを使ってなければならない',
            THEME_INKJET: '3\, 2\, 1\...飛び立つ！',
            THEME_INKJET_DESCRIPTION: 'みんなはジェットパックを持っている武器を使ってなければならない',
            THEME_STINGRAY: 'メガホンレーザーじゃない',
            THEME_STINGRAY_DESCRIPTION: 'みんなはハイパープレッサーを持っている武器を使ってなければならない',
            THEME_BRUSH: 'デッキを拭く',
            THEME_BRUSH_DESCRIPTION: 'みんなはパブロとかホクサイとか使ってなければならない',
            THEME_AEROSPRAY: 'プロになる！',
            THEME_AEROSPRAY_DESCRIPTION: 'みんなはプロモデラーを使ってなければならない',
            THEME_DYNAMO: 'ダイナモ絶対に許さん!',
            THEME_DYNAMO_DESCRIPTION: 'みんなはダイナモローラーを使ってなければならない',
            THEME_BALLER: 'スフィアサーキット',
            THEME_BALLER_DESCRIPTION: 'みんなはイカスフィアを持っている武器を使ってなければならない',
            THEME_NINJA: '忍者ダニーくん 2',
            THEME_NINJA_DESCRIPTION: 'ダニーを暗殺させろう！みんなはイカニンジャ服とステルスジャンプ靴を着てなければならない。',
            THEME_AUTOBOMB: 'ロボットボム出撃!',
            THEME_AUTOBOMB_DESCRIPTION: 'みんなはロボットボムを持っている武器を使ってなければならない',
            THEME_BURSTBOMB: 'クイボ痛い',
            THEME_BURSTBOMB_DESCRIPTION: 'みんなはクイックボムを持っている武器を使ってなければならない',
            THEME_BUCKETS: 'Bバスバケツ',
            THEME_BUCKETS_DESCRIPTION: 'みんなはスロッシャー武器を使ってなければならない',           
            THEME_RAIN: 'てるてる坊主',
            THEME_RAIN_DESCRIPTION: 'みんなはアメフラシを持っている武器を使ってなければならない',
            THEME_BANNED: '禁止ステージとルール',
            THEME_BANNED_DESCRIPTION: 'このマップとルールコンビはガチマッチ出ないから遊ぼう！',
            THEME_ROLLER: 'ローラー４エヴァー！ 2',
            THEME_ROLLER_DESCRIPTION: 'みんなはローラーを使ってなければならない。そしてころころだけ。<br />いいか？ころころするだけだ。<br /><br /><br />いいいいいかああああ？ころころだけだ！だけ！だ！<br /><small>hikaruでちゅはひかれた。</small><br /><br /><strong>＊壁を塗るために振るのが大丈夫だ</strong>',
            THEME_BUMPER: 'ぶつかれ筆２',
            THEME_BUMPER_DESCRIPTION: 'みんなはパブロとホクサイを使ってなければならない。振るのが禁止！ころころするだけできる',
            THEME_SPLATLING: 'hikaruでちゅのユートピア２',
            THEME_SPLATLING_DESCRIPTION: 'みんなはスピナーを使ってなければならない。スピナーがあるから感謝しよう！',
            THEME_INKMINE: 'それは罠だ',
            THEME_INKMINE_DESCRIPTION: 'みんなはトラップを持っている武器を使ってなければならない',
            THEME_NOZZLENOSE: 'リール芸術',
            THEME_NOZZLENOSE_DESCRIPTION: 'みんなはリールガンを使ってなければならない',
            THEME_SPRINKLERS: 'スプリンクラー美術大学',
            THEME_SPRINKLERS_DESCRIPTION: 'みんなはスプリンクラーを持っている武器を使ってなければならない',
            THEME_ELITRES: 'ダニーの悪夢',
            THEME_ELITRES_DESCRIPTION: 'みんなはリッターを使ってなければならない',
            THEME_BLASTERS: '花火！',
            THEME_BLASTERS_DESCRIPTION: 'みんなはブラースタを使ってなければならない',
            THEME_BOMBRUSH: 'トキメキ ボムラッシュ２',
            THEME_BOMBRUSH_DESCRIPTION: 'みんなはボムラッシュ(スプラッシュとキューバンとクイックとカーリング)を持っている武器を使ってなければならない',
            THEME_SPLATTERSHOTJR: 'まうす練習',
            THEME_SPLATTERSHOTJR_DESCRIPTION: 'まうすのように考える。まうすのように綺麗に。まうすになろう！<br /><br />みんなはわかばシューターを使ってなければならない',
            THEME_SPLOOSHNEO: 'ダイオウイカ海峡大橋',
            THEME_SPLOOSHNEO_DESCRIPTION: 'みんなはわかばボールドマーカーネオを使ってなければならない',
            THEME_BEACONS: 'イカジャンプゲーム２',
            THEME_BEACONS_DESCRIPTION: 'みんなはジャンプビーコンを 持っている武器を使ってなければならない',
            THEME_INKZOOKA: 'スーパーショット炭鉱',
            THEME_INKZOOKA_DESCRIPTION: 'みんなはスーパーショットを持っている武器を使ってなければならない',
            THEME_INKSTRIKE: 'トルネードとどけ！！！',
            THEME_INKSTRIKE_DESCRIPTION: 'みんなはトルネードを持っている武器を使ってなければならない',
            THEME_NAKOCHI: 'みんなはなこち！',
            THEME_NAKOCHI_DESCRIPTION: '起きた時になこちになって気づいた！あれの人も…なこちになった！みんなはなこちだ！ナコチウォーズが始まった…<br /><br />ギアを着てなければならない:<ul><li>頭: キャディ サンバイザー</li><li>服: ボーダーネイビー</li><li>靴: アケビコンフォート</li></ul>',
        })
        .registerAvailableLanguageKeys(['en', 'jp'], {
            'en_*': 'en',
            'jp_*': 'jp',
            '*': 'en'
        })
        .determinePreferredLanguage()
        .fallbackLanguage('en');
}]);

splatApp.controller('SplatoonController', ['$scope',
    '$translate',
    function ($scope, $translate) {
        $scope.Title = "UI_SITETITLE";

        for (var i = 0; i < stages.length; i++) {
            stages[i].Number = i + 1;
            stages[i].Image = "";

            var stageIdentifier = stages[i].Name.toLowerCase().replace("stage_", "");
            stages[i].Image = IMAGE_ROOT + stageIdentifier + ".png";
        }

        $scope.stages = stages;

        $scope.toggleLanguage = function (key) {
            var languageCode = $translate.use();
            if (languageCode === 'jp') {
                $translate.use('en');
            } else {
                $translate.use('jp');
            }
        };
    }
]);